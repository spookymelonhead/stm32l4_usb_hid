#include "main.h"
#include "stm32l4xx_hal.h"
#include "usb_device.h"
#include "usbd_hid.h"
#include "stdbool.h"

void SystemClock_Config(void);
static void MX_GPIO_Init(void);


#define INC_VALUE 5
extern USBD_HandleTypeDef hUsbDeviceFS;

uint8_t hid_buffer[4]={0,0,0,0};
uint8_t x_sweep = 0x00;
uint8_t y_sweep = 0xFF;

void USB_HID_send_report();
void update_HID_data();

#define TRUE (1)
#define FALSE (0)

uint8_t usb_hid_transmit_buffer[60U] =
{
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
		11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
		21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
		31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
		41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
		51, 52, 53, 54, 55, 56, 57, 58, 59
};

typedef struct
{
	union
	{
		uint32_t timestamp;
		struct
		{
			uint8_t timestamp_ar[4U];
		}timestamp_s;;

	}timestamp_u;

}timestamp_t;

timestamp_t time;

uint8_t usb_hid_tx_buf[64];

int main(void)
{
	time.timestamp_u.timestamp = 0U;

  HAL_Init();

  SystemClock_Config();

  MX_GPIO_Init();
  MX_USB_DEVICE_Init();

  while (1)
  {
  }
}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 40;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLLSAI1;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_MSI;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 24;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_48M2CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)

  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the main internal regulator output voltage 
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin : UP_Pin */
  GPIO_InitStruct.Pin = UP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(UP_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}


uint32_t last_tick_sr = 0U;
const uint32_t period_sr = 1U;
bool new_data = FALSE;
uint32_t counter_new_data_fail = 0U;


void USB_HID_send_report()
{
	uint32_t curr_tick = HAL_GetTick();
	uint8_t status = 0;
 	if(curr_tick - last_tick_sr >= period_sr)
 	{
 		time.timestamp_u.timestamp = curr_tick;


// 		usb_hid_tx_buf[0] =  time.timestamp_u.timestamp_s.timestamp_ar[0];
// 		usb_hid_tx_buf[1] =  time.timestamp_u.timestamp_s.timestamp_ar[1];
// 		usb_hid_tx_buf[2] =  time.timestamp_u.timestamp_s.timestamp_ar[2];
// 		usb_hid_tx_buf[3] =  time.timestamp_u.timestamp_s.timestamp_ar[3];
// 		memcpy(usb_hid_tx_buf + 4,  usb_hid_transmit_buffer + 4, 60);

 		hid_buffer[0]= 0b00000111;

		  hid_buffer[1]= x_sweep;
		  hid_buffer[2]= y_sweep;

		  status = USBD_HID_SendReport(&hUsbDeviceFS, time.timestamp_u.timestamp_s.timestamp_ar, 4);
		  if(status != 0U)
			  exit(0);
		  else usb_hid_transmit_buffer[64U - 1U]  = 0U;
		  last_tick_sr = curr_tick;
	}
}

uint32_t last_tick_ud = 0U;
const uint32_t sampling_rate = 8U; //8ms 125Hz
uint32_t data_sent = 0x00;
uint32_t sample_send_fail = 0U;
uint8_t counterrr = 0U;
void update_HID_data()
{
	uint32_t curr_tick = HAL_GetTick();


	if(curr_tick- last_tick_ud >= sampling_rate)
	{
		x_sweep++;
		y_sweep--;

		if(0xFF == x_sweep)
		{
			x_sweep = 0x00;
		}
		if(0x00 == y_sweep)
		{
			y_sweep = 0xFF;
		}
		if(0x00 == data_sent)
		{
			sample_send_fail++;
		}
		else if(0xFF == data_sent)
		{
			data_sent = 0x00;
		}
		else
		{
			int a;
		}


		if(0xFF == counterrr ) counterrr = 0U;
		counterrr++;


		usb_hid_transmit_buffer[64U - 1U]  = counterrr;

		  last_tick_ud = curr_tick;
	}

}


void HAL_SYSTICK_Callback(void)
{
	update_HID_data();
	USB_HID_send_report();
}



#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

