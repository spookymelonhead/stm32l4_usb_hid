# README #
USB HID Communication b/w Linux(Host, uses HIDAPI) and STM32L476RGNucleo(Device, uses STM's USB stack).
Device sends timestamp(4Bytes) every 1ms to host.
Device tries to send timestamp(4Bytes) + 60Bytes of other data every 1ms to host, but successfully happens only at about 16ms(16 x time it took to transfer 4 Bytes).
Core@80MHz and USB@48MHz.
hidapi/c_hidtest.sh ----> compile and run host side code.

### What is this repository for? ###

* Quick summary: USB HID Communication b/w Linux(Host, uses HIDAPI) and STM32L476RGNucleo(Device, uses STM's USB stack)
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Contact ###

Author: Brian doofus
Email: hardik.mad@gmail.com
Blog: https://tachymoron.wordpress.com* Repo owner or admin